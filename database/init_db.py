import json
import yaml
import aiohttp
import aiofiles

from sqlalchemy import create_engine
from aiomysql.sa import create_engine as aio_create_engine
from .models import User, Unit


async def read_configs():
    async with aiofiles.open('configs/database.yaml', 'r') as file:
        file_data = await file.read()
    return yaml.load(file_data)


async def fetch(session, url: str):
    async with session.get(url) as response:
        return await response.text()


async def get_data(data: str):
    async with aiohttp.ClientSession() as session:
        html = await fetch(session, 'http://admin.geliospro.com/sdk/?login=demo&pass=demo&svc=get_%s&params={}' % data)
        return json.loads(html)


async def is_exists(engine, table: str, id: int):
    test_query = f"SELECT * FROM {table} WHERE id = '{id}';"
    async with engine.acquire() as conn:
        row = await conn.execute(test_query)
        return bool(row.rowcount)


async def insert_users(engine):
    data = await get_data('users')

    query = User.__table__.insert()
    for elem in data:
        query = query.values(
            id=elem['id'],
            login=elem['login'],
            is_admin=elem['is_admin'],
            auth_key=elem['auth_key'],
            address_base=elem['address_base']
        )

        if not await is_exists(engine, 'users', elem['id']):
            async with engine.acquire() as conn:
                await conn.execute(query)


async def insert_units(engine):
    data = await get_data('units')

    query = Unit.__table__.insert()
    for elem in data:
        query = query.values(
            id=elem['id'],
            name=elem['name'],
            creator=elem['creator']
        )

        if not await is_exists(engine, 'units', elem['id']):
            async with engine.acquire() as conn:
                await conn.execute(query)


async def insert_data(engine):
    await insert_users(engine)
    await insert_units(engine)


async def create_tables():
    configs = await read_configs()
    _engine = create_engine(
        'mysql+{}://{}:{}@{}/{}?charset=utf8'.format(
            configs['mysql']['engine'],
            configs['mysql']['user'],
            configs['mysql']['password'] if configs['mysql']['password'] else '',
            configs['mysql']['host'],
            configs['mysql']['database']),
        pool_recycle=int(configs['mysql']['port']),
        echo=True
    )

    if not _engine.dialect.has_table(_engine, 'users'):
        User.__table__.create(_engine)

    if not _engine.dialect.has_table(_engine, 'units'):
        Unit.__table__.create(_engine)


async def init_database():
    configs = await read_configs()

    engine = await aio_create_engine(
        db=configs['mysql']['database'],
        user=configs['mysql']['user'],
        password=configs['mysql']['password'],
        host=configs['mysql']['host'],
        port=int(configs['mysql']['port']),
        autocommit=True
    )

    await create_tables()
    await insert_data(engine)

    return engine
