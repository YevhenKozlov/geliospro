from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Boolean, Integer, String

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    login = Column(String(32), unique=True)
    is_admin = Column(Boolean)
    auth_key = Column(String(32), unique=True)
    address_base = Column(String(256))


class Unit(Base):
    __tablename__ = 'units'

    id = Column(Integer, primary_key=True)
    name = Column(String(64))
    creator = Column(Integer)
