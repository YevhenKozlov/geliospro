import aiohttp_jinja2

from aiohttp import web
from database.models import User, Unit
from database.init_db import init_database

routes = web.RouteTableDef()


@routes.get('/get_users')
@aiohttp_jinja2.template('users.html')
async def get_users(request):
    engine = await init_database()
    async with engine.acquire() as conn:
        cursor = await conn.execute(User.__table__.select())
        records = await cursor.fetchall()
        data = [dict(elem) for elem in records]
    return {'users': data}


@routes.get('/get_units')
@aiohttp_jinja2.template('units.html')
async def get_units(request):
    engine = await init_database()
    async with engine.acquire() as conn:
        cursor = await conn.execute(Unit.__table__.select())
        records = await cursor.fetchall()
        data = [dict(elem) for elem in records]
    return {'units': data}
