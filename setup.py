import os
import aiohttp_jinja2
import asyncio
import jinja2

from aiohttp import web
from polls.views import routes


loop = asyncio.get_event_loop()

app = web.Application(loop=loop)
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.getcwd() + '/polls/templates'))
app.add_routes(routes)
web.run_app(app)
